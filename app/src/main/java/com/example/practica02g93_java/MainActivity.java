package com.example.practica02g93_java;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button btnCalcular, btnLimpiar, btnRegresar;
    private EditText txtPeso, txtAltura, txtResultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnRegresar = (Button) findViewById(R.id.btnRegresar);
        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        txtAltura = (EditText) findViewById(R.id.txtAltura);
        txtPeso = (EditText) findViewById(R.id.txtPeso);
        txtResultado = (EditText) findViewById(R.id.txtResultado);


        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtResultado.setText("");
                txtPeso.setText("");
                txtAltura.setText("");
                txtAltura.requestFocus();
                txtPeso.requestFocus();
            }
        });
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtAltura.getText().toString().isEmpty() || txtPeso.getText().toString().isEmpty())
                {
                    Toast.makeText(MainActivity.this, "Captura en todos los campos", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    float Altura = Float.parseFloat(txtAltura.getText().toString()); //Captura de valores
                    float Peso = Float.parseFloat(txtPeso.getText().toString());

                    float IMC = Peso / (Altura * Altura);

                    String Resultado = String.format("%.2f", IMC);

                    txtResultado.setText(Resultado);

                }
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}