package com.example.practica02g93_java;

import java.util.Scanner;

public class CalcularIMC {

    public static void main(String[] args) {

        Scanner val = new Scanner(System.in);

        System.out.print("Calculadora de IMC");

        System.out.print("Ingresa tu peso en Kilos(KG):");
        float Peso = val.nextFloat();

        System.out.print("Ingresa tú altura en Metros (M):");
        float Altura = val.nextFloat();

        float IMC = CalcularIMC(Peso, Altura);
        System.out.print("IMC: " + IMC);

        System.out.print("Gracias por usar la calculadora");
    }

    public static float CalcularIMC(float Peso, float Altura){
        return Peso / (Altura * Altura);
    }
}
